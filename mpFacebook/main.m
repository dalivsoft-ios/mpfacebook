//
//  main.m
//  mpFacebook
//
//  Created by Vitaly Evtushenko on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ASAppDelegate class]));
    }
}
