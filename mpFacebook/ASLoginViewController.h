//
//  ASLoginViewController.h
//  mpFacebook
//
//  Created by Gleb on 24.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASViewController.h"

@interface ASLoginViewController : ASViewController

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIImageView *botAlert;

- (IBAction)loginButtoButtonDidClicked:(id)sender;


@end
