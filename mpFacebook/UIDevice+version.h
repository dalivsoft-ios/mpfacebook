//
//  UIDevice+version.h
//  sweetgift-ipad
//
//  Created by Vitaly Evtushenko on 25.10.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IOS_VERSION_AT_LEAST(version) [[UIDevice currentDevice] iOSVersionIsAtLeast:version]

@interface UIDevice (version)

- (BOOL)iOSVersionIsAtLeast:(NSString*)version;

@end
