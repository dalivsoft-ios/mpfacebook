//
//  ASURLCache.m
//  autoparad
//
//  Created by Vitaly Evtushenko on 08.11.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import "ASURLCache.h"
#import "NSString+MD5Addition.h"

@implementation ASURLCache
{
    NSString *_path;
}

- (void)createCachesDir
{
    NSString *cahesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSFileManager *fm = [NSFileManager defaultManager];
    BOOL isDirectory = YES;
    _path = [cahesPath stringByAppendingPathComponent:@"__as_url_cache__"];
    if (![fm fileExistsAtPath:_path isDirectory:&isDirectory]) {
        [fm createDirectoryAtPath:_path withIntermediateDirectories:YES attributes:nil error:nil];
    }
}

- (id)initWithMemoryCapacity:(NSUInteger)memoryCapacity diskCapacity:(NSUInteger)diskCapacity diskPath:(NSString *)path
{
    if ((self = [super initWithMemoryCapacity:memoryCapacity diskCapacity:diskCapacity diskPath:path])) {
        _path = path;
        if (!_path) {
            [self createCachesDir];
        }
    }
    return self;
}

- (NSCachedURLResponse *)cachedResponseForRequest:(NSURLRequest *)request
{
    if (![request.HTTPMethod isEqualToString:@"GET"] && ![request.HTTPMethod isEqualToString:@"POST"]) return nil;
    NSString *path = [self cachePathForRequest:request];
    NSFileManager *fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:path]) return nil;
    NSDictionary *response = (NSDictionary *)[NSKeyedUnarchiver unarchiveObjectWithFile:path];
    NSCachedURLResponse *cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:response[@"response"] data:response[@"data"] userInfo:nil storagePolicy:NSURLCacheStorageAllowed];
    return cachedResponse;
}

- (NSString *)cachePathForRequest:(NSURLRequest *)request
{
    NSString *md5 = [request.URL.absoluteString md5];
    if ([request.HTTPMethod isEqualToString:@"POST"]) {
        md5 = [[md5 stringByAppendingString:[request.HTTPBody md5]] md5];
    }
    NSString *path = _path;
    NSFileManager *fm = [NSFileManager defaultManager];
    NSInteger levels = 4;
    for (int i=0; i<levels; ++i) {
        NSRange r = NSMakeRange(i, 1);
        path = [path stringByAppendingPathComponent:[md5 substringWithRange:r]];
    }
    if (![fm fileExistsAtPath:path]) {
        [fm createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSRange r = NSMakeRange(levels, md5.length - levels);
    path = [path stringByAppendingPathComponent:[md5 substringWithRange:r]];
    return path;
}

- (void)storeCachedResponse:(NSCachedURLResponse *)cachedResponse forRequest:(NSURLRequest *)request
{
    if (![request.HTTPMethod isEqualToString:@"GET"] && ![request.HTTPMethod isEqualToString:@"POST"]) return;
    NSString *path = [self cachePathForRequest:request];
    [NSKeyedArchiver archiveRootObject:@{@"response" : cachedResponse.response, @"data" : cachedResponse.data}
                                toFile:path];
}

- (void)removeCachedResponseForRequest:(NSURLRequest *)request
{
    if (![request.HTTPMethod isEqualToString:@"GET"] && ![request.HTTPMethod isEqualToString:@"POST"]) return;
    NSString *path = [self cachePathForRequest:request];
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

- (void)removeAllCachedResponses
{
    NSFileManager *fm = [NSFileManager defaultManager];
    [fm removeItemAtPath:_path error:nil];
    [self createCachesDir];
}

@end
