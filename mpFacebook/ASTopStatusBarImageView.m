//
//  ASTopStatusBarImageView.m
//  mpFacebook
//
//  Created by Gleb on 27.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//
#define pickUp 20;

#import "ASTopStatusBarImageView.h"

@implementation UILabel (setFont)

-(void)setFontSize
{
    CGSize strSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(MAXFLOAT, self.frame.size.height)];
    float fontSize = self.font.pointSize;
    if(strSize.width > 118){
        while (strSize.width > self.frame.size.width) {
            fontSize --;
            strSize = [self.text sizeWithFont:[UIFont boldSystemFontOfSize:fontSize] constrainedToSize:CGSizeMake(MAXFLOAT, self.frame.size.height)];
        }
    }
    self.font = [UIFont boldSystemFontOfSize:fontSize];
}

@end

@implementation ASTopStatusBarImageView

- (id)init
{
    self =[super initWithImage:[UIImage imageNamed:@"topImage"]];
    if (self) {
                
        //logoText
        self.statusLabel = [UILabel new];
        self.statusLabel.backgroundColor = [UIColor clearColor];
        self.statusLabel.textColor = [UIColor whiteColor];
        self.statusLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.statusLabel];
        
        self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logoText"]];
        [self addSubview:self.logoImageView];
        self.isLogoEnabled = YES;
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.statusLabel.frame = self.bounds;
    float xOffset,yOffset;
    xOffset = (self.frame.size.width - self.logoImageView.frame.size.width) / 2;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        yOffset = ((self.frame.size.height - self.logoImageView.frame.size.height) / 2)-pickUp; //
    } else {
        yOffset = ((self.frame.size.height - self.logoImageView.frame.size.height) / 2); //
    }
    self.logoImageView.frame = CGRectMake(xOffset, yOffset, self.logoImageView.frame.size.width, self.logoImageView.frame.size.height);
}

- (void)setStatus:(NSString *)status
{
    _status = status;
    if(status){
        self.statusLabel.font = [UIFont boldSystemFontOfSize:45];
        self.statusLabel.frame = self.bounds;
        if(self.statusLabel){
            self.isLogoEnabled = NO;
            self.statusLabel.text = status;
            [self.statusLabel setFontSize];
        }
    }
}

-(void)setIsLogoEnabled:(BOOL)isLogoEnabled
{
    _isLogoEnabled = isLogoEnabled;
    
    if(_isLogoEnabled){
        self.logoImageView.hidden = NO;
    }else{
        self.logoImageView.hidden = YES;
    }
}

@end
