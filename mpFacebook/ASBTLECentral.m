//
//  ASBTLECentral.m
//  mpFacebook
//
//  Created by Gleb on 16.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASBTLECentral.h"
#import "ASCoreServices.h"
#import "ASBTLEManager.h"
#import "ASBTLEPeripheral.h"
#import "ASMiniFacebookProfile.h"
#import "ASFacebookManager.h"
#import "ASAppDelegate.h"

@implementation ASBTLECentral
{
    NSMutableArray *_duplicatedPeripherals;
    CBPeripheral *peripheralToConnect;
    NSString *_deviceUdidCanceledAvatarLoading;
    NSString *_response;
}
- (id)init
{
    self = [super init];
    if (self) {
        self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        _duplicatedPeripherals = [NSMutableArray new];
    }
    return self;
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state != CBCentralManagerStatePoweredOn) {
        return;
    }
}


- (void)startScan
{
    self.connectedPeripheral = nil;
    [self.centralManager scanForPeripheralsWithServices:nil
                                                options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
    self.mode = ASBTLECentralModeScann;
    ASLog(@"Scanning started");
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    ASLog(@"Discovered %@ at %@", peripheral.name, RSSI);
    
    if (self.mode == ASBTLECentralModeScann) {
        [self.centralManager stopScan];
        self.connectedPeripheral = peripheral;
        if(peripheral.UUID != NULL){
            if(![[[[ASBTLEManager sharedManager] peripheralsArray] valueForKey:@"deviceUUID"] containsObject:[self _UUIDToString: peripheral.UUID]] && ![_duplicatedPeripherals containsObject:[self _UUIDToString: peripheral.UUID]]){
                self.connectedPeripheral = peripheral;
                [self.centralManager connectPeripheral:self.connectedPeripheral options:nil];
                [self startConnectionTimeoutMonitor:peripheral];
            }
        }else{
            self.connectedPeripheral = peripheral;
            [self.centralManager connectPeripheral:self.connectedPeripheral options:nil];
            [self startConnectionTimeoutMonitor:peripheral];
        }
    }
}

#pragma mark -

- (void)startConnectionTimeoutMonitor:(CBPeripheral *)peripheral {
    [self cancelConnectionTimeoutMonitor:peripheral];
    [self performSelector:@selector(connectionDidTimeout:)
               withObject:peripheral
               afterDelay:11.0];
}

- (void)cancelConnectionTimeoutMonitor:(CBPeripheral *)peripheral {
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(connectionDidTimeout:)
                                               object:peripheral];
}

- (void)connectionDidTimeout:(CBPeripheral *)peripheral {
    NSLog(@"connectionDidTimeout: %@", peripheral.UUID);
    [self cleanup];
}

#pragma mark -
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    ASLog(@"Failed to connect to %@. (%@)", peripheral, [error localizedDescription]);
    [self cleanup];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    ASLog(@"Peripheral Connected");
            [self.centralManager stopScan];
            peripheral.delegate = self;
            [peripheral discoverServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering services: %@", [error localizedDescription]);
        //[[[UIAlertView alloc] initWithTitle:@"Unknown error" message:@"try to reset BTLE" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil] show];
        [self cleanup];
        return;
    }
    peripheral.delegate = self;
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:PROFILE],[CBUUID UUIDWithString:ADD_FRIEND_REQUEST],[CBUUID UUIDWithString:ADD_FRIEND_RESPONSE]] forService:service];
    }
}


- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        [self cleanup];
        return;
    }
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        if(self.mode == ASBTLECentralModeReadData || self.mode == ASBTLECentralModeScann){
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:PROFILE]]) {
                [peripheral readValueForCharacteristic:characteristic];
            }
        }
        if(self.mode == ASBTLECentralModeWriteData){
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:ADD_FRIEND_REQUEST]]) {
                NSData* expectedData = [[[ASFacebookManager sharedManager] myProfile] profileData];
                int length = [expectedData length];
                int currentOffset = 0;
                NSData *dataToSend;
                while (currentOffset <= length) {
                    if(length - currentOffset <= 19){
                          dataToSend = [expectedData subdataWithRange:NSMakeRange(currentOffset, length - currentOffset)];
                    }
                    else{
                        dataToSend = [expectedData subdataWithRange:NSMakeRange(currentOffset, 19)];
                    }
                       NSLog(@"%@ !!! %@ !!! %d",dataToSend,[[NSString alloc] initWithData:dataToSend encoding:NSUTF8StringEncoding],[dataToSend length]);
                    [peripheral writeValue:dataToSend forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
                    currentOffset = currentOffset + 19;
                }
                 [peripheral writeValue:[@"EOM" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
                
                [self performSelector:@selector(cleanup) withObject:nil afterDelay:1.0f];
            }
        }
        if(self.mode == ASBTLECentralModeSendingResponse){
            [peripheral writeValue:[_response dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            //[self cleanup];
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (self.mode == ASBTLECentralModeSendingResponse){
        [self cleanup];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        [self cleanup];
        return;
    }
        ASMiniFacebookProfile *profile = [[ASMiniFacebookProfile alloc] initWithRecivedData:characteristic.value];
        profile.deviceUUID = [self _UUIDToString:peripheral.UUID];
        
        if(profile){
            if(profile.udid){
                if(![[[[ASBTLEManager sharedManager] peripheralsArray] valueForKey:@"udid"] containsObject:profile.udid]){
                    self.newDeviseDetected(profile);
                    [_duplicatedPeripherals addObject:[self _UUIDToString: peripheral.UUID]];
                }
            }else{
                self.newDeviseDetected(profile);
            }
        }
    [self performSelector:@selector(cleanup) withObject:nil afterDelay:1.0f];
}

#pragma mark usefull stuff

- (NSString*) _UUIDToString:(CFUUIDRef)UUID
{
    if (!UUID)
        return nil;
    
    CFStringRef s = CFUUIDCreateString(NULL, UUID);
    
   return [[NSString alloc] initWithUTF8String:CFStringGetCStringPtr(s, 0)];
}


- (int) compareCBUUID:( CFUUIDRef) UUID1 UUID2:(CFUUIDRef)UUID2
{
    CBUUID *CBUUID1 = [CBUUID UUIDWithCFUUID:UUID1];
    CBUUID *CBUUID2 = [CBUUID UUIDWithCFUUID:UUID2];
    char b1[16];
    char b2[16];
    [CBUUID1.data getBytes:b1];
    [CBUUID2.data getBytes:b2];
    if (memcmp(b1, b2, CBUUID1.data.length) == 0)return 1;
    else return 0;
}

#pragma mark

- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral
{
    [self cleanup];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Peripheral Disconnected");
    [self cleanup];
}

- (void)cleanup
{
    [self stopScan];
    if (!self.connectedPeripheral.isConnected) {
        [self startScan];
        return;
    }
    if (self.connectedPeripheral.services != nil) {
        for (CBService *service in self.connectedPeripheral.services) {
            if (service.characteristics != nil) {
                for (CBCharacteristic *characteristic in service.characteristics) {
                    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:PROFILE]]) {
                        if (characteristic.isNotifying) {
                        [self.connectedPeripheral setNotifyValue:NO forCharacteristic:characteristic];
                            return;
                        }
                    }
                }
            }
        }
    }
    
    [self.centralManager cancelPeripheralConnection:self.connectedPeripheral];
    self.connectedPeripheral = nil;
    self.mode = ASBTLECentralModeScann;
    [self startScan];
}

- (void)disconectCurrentConnection
{
    if(self.connectedPeripheral){
        [self.centralManager cancelPeripheralConnection:self.connectedPeripheral];
    }
}

#pragma mark direct connectiom

- (void)readDataFromDeviceWithUdid:(NSString*)udid
{
    if (udid){
        self.mode = ASBTLECentralModeReadData;
        [self.centralManager retrievePeripherals:@[udid]];
    }
}

- (void)writeDataToDeviceWithUdid:(NSString*)udid
{
    if (udid){
        self.mode = ASBTLECentralModeWriteData;
        CFUUIDRef uuidrefs = CFUUIDCreateFromString(NULL, (CFStringRef)udid);
        [self.centralManager retrievePeripherals:@[(__bridge id)uuidrefs]];
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    if(peripherals.count == 0) return;
    CBPeripheral *peripheral = peripherals[0];
    peripheralToConnect = peripheral;
    if(peripheral.isConnected){
        peripheral.delegate = self;
        [peripheral discoverServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]];
    }
    else{
        [self cancelConnectionTimeoutMonitor:peripheral];
        [central connectPeripheral:peripheral options:nil];
    }
}

#pragma mark response

- (void)sendResponseWithAccept:(BOOL)isAccept uudid:(NSString *)uudid
{
    if(uudid){
        if(isAccept){
            _response = @"1";
        } else {
            _response = @"0";
        }
        [[ASBTLEManager sharedManager] reciveAnsvwerFromUser:uudid withResult:_response];
        self.mode = ASBTLECentralModeSendingResponse;
         CFUUIDRef uuidrefs = CFUUIDCreateFromString(NULL, (CFStringRef)uudid);
        [self.centralManager retrievePeripherals:@[(__bridge id)uuidrefs]];
    }
}

#pragma mark

#pragma mark stop scan
- (void)stopScan
{
    if(self.connectedPeripheral){
        [self.centralManager cancelPeripheralConnection:self.connectedPeripheral];
    }
    [self.centralManager stopScan];
    ASLog(@"Scanning Stoped");
}

- (void)_refresh
{
    [[ASBTLEManager sharedManager] disbleCenralMode];
    [[ASBTLEManager sharedManager] enableCenralMode];

}

@end
