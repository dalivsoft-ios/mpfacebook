//
//  ASNetworkLog.m
//  ASCoreServices
//
//  Created by Vitaly Evtushenko on 08.05.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import "ASNetworkLog.h"
#import <Foundation/Foundation.h>

#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

#import <CommonCrypto/CommonDigest.h>
#import "UIDevice-Hardware.h"
#import "Reachability.h"
#import "ASHTTPRequest.h"

static ASNetworkLog *sharedInstance;
static NSString *s_uuid;
static NSString *s_devID;

@implementation ASNetworkLog

+ (ASNetworkLog *)sharedLog {
    return sharedInstance;
}

+ (void)initialize {
    NSAssert(self == [ASNetworkLog class], @"Singleton is not designed to be subclassed.");
    sharedInstance = [[ASNetworkLog alloc] init];
}

- (id)init {
    if ((self = [super init])) {
        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        CFStringRef uuidStr = CFUUIDCreateString(kCFAllocatorDefault, uuidRef);
        uuid = [[NSString alloc] initWithString:(__bridge NSString *)uuidStr];
        CFRelease(uuidStr);
        CFRelease(uuidRef);
    }
    return self;
}

// Return the local MAC addy
// Courtesy of FreeBSD hackers email list
// Accidentally munged during previous update. Fixed thanks to erica sadun & mlamb.
- (NSString *) macaddress{
    
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        free(buf);
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X", 
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    
    return outstring;
}

- (NSString *) stringFromMD5:(NSString*)md5{
    
    if(md5 == nil || [md5 length] == 0)
        return nil;
    
    const char *value = [md5 UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return outputString;
}

- (NSString *) uniqueGlobalDeviceIdentifier{
    NSString *uniqueIdentifier;
    if([[[UIDevice currentDevice] systemVersion] floatValue] > 5)
    {
        uniqueIdentifier =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    }else{
        NSString *macaddress = [self macaddress];
        uniqueIdentifier = [self stringFromMD5:macaddress];
    }
    
    return uniqueIdentifier;
}

void myExceptionHandler (NSException *exception)
{
    NSString *message = [NSString stringWithFormat:@"CRASH!!!\n\nName: %@\n\nReason: %@\n\nUser info: %@\n\nStack symbols: %@", [exception name], [exception reason], [exception userInfo], [exception callStackSymbols]];
    
    NSLog(@"%@", message);
    
    Reachability *inet = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [inet currentReachabilityStatus];
    if (NotReachable == status) return;
    
    ASHTTPRequest *request = [ASHTTPRequest requestWithURL:[NSURL URLWithString:@"http://debug.ashberrysoft.com/addlog.php"]];
    request.requestMethod = @"POST";
    [request addPostValue:s_devID forKey:@"device[id]"];
    [request addPostValue:message forKey:@"message"];
    [request addPostValue:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundle[id]"];
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    [request addPostValue:[info valueForKey:@"CFBundleShortVersionString"] forKey:@"bundle[version]"];
    [request addPostValue:[info valueForKey:@"CFBundleName"] forKey:@"bundle[name]"];
    [request addPostValue:[info valueForKey:@"CFBundleDisplayName"] forKey:@"bundle[displayName]"];
    UIDevice *dev = [UIDevice currentDevice];
    [request addPostValue:[dev name] forKey:@"device[name]"];
    [request addPostValue:[dev model] forKey:@"device[model]"];
    [request addPostValue:[dev localizedModel] forKey:@"device[localizedModel]"];
    [request addPostValue:[dev systemName] forKey:@"device[systemName]"];
    [request addPostValue:[dev systemVersion] forKey:@"device[systemVersion]"];
    [request addPostValue:[dev platform] forKey:@"device[platform]"];
    [request addPostValue:[dev hwmodel] forKey:@"device[hwmodel]"];
    [request addPostValue:[dev platformString] forKey:@"device[platformString]"];
    [request addPostValue:[dev macaddress] forKey:@"device[macaddress]"];
    [request addPostValue:[dev platform] forKey:@"device[platform]"];
    [request addPostValue:s_uuid forKey:@"session"];
    [request addPostValue:[NSString stringWithFormat:@"%d", ReachableViaWiFi == status] forKey:@"wifi"];
    [request addPostValue:[NSString stringWithFormat:@"%d", [dev isJailBroken]] forKey:@"device[jailbreak]"];
    [request startSynchronous];
}

- (void)log:(NSString *)message {
    if (!crashHandlerInstalled) {
        NSSetUncaughtExceptionHandler(&myExceptionHandler);
        crashHandlerInstalled = YES;
    }
    
    s_uuid = uuid;
    s_devID = [self uniqueGlobalDeviceIdentifier];
    
    Reachability *inet = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [inet currentReachabilityStatus];
    
    if (NotReachable == status) return;
    
    ASHTTPRequest *request = [ASHTTPRequest requestWithURL:[NSURL URLWithString:@"http://debug.ashberrysoft.com/addlog.php"]];
    request.requestMethod = @"POST";
    [request addPostValue:[self uniqueGlobalDeviceIdentifier] forKey:@"device[id]"];
    [request addPostValue:message forKey:@"message"];
    [request addPostValue:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundle[id]"];
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    [request addPostValue:[info valueForKey:@"CFBundleVersion"] forKey:@"bundle[version]"];
    [request addPostValue:[info valueForKey:@"CFBundleName"] forKey:@"bundle[name]"];
    [request addPostValue:[info valueForKey:@"CFBundleDisplayName"] forKey:@"bundle[displayName]"];
    UIDevice *dev = [UIDevice currentDevice];
    [request addPostValue:[dev name] forKey:@"device[name]"];
    [request addPostValue:[dev model] forKey:@"device[model]"];
    [request addPostValue:[dev localizedModel] forKey:@"device[localizedModel]"];
    [request addPostValue:[dev systemName] forKey:@"device[systemName]"];
    [request addPostValue:[dev systemVersion] forKey:@"device[systemVersion]"];
    [request addPostValue:[dev platform] forKey:@"device[platform]"];
    [request addPostValue:[dev hwmodel] forKey:@"device[hwmodel]"];
    [request addPostValue:[dev platformString] forKey:@"device[platformString]"];
    [request addPostValue:[dev macaddress] forKey:@"device[macaddress]"];
    [request addPostValue:[dev platform] forKey:@"device[platform]"];
    [request addPostValue:uuid forKey:@"session"];
    [request addPostValue:[NSString stringWithFormat:@"%d", ReachableViaWiFi == status] forKey:@"wifi"];
    [request addPostValue:[NSString stringWithFormat:@"%d", [dev isJailBroken]] forKey:@"device[jailbreak]"];
    
    [request startAsynchronous];
}

@end

