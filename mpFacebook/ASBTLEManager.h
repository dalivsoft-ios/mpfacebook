//
//  ASBTLEManager.h
//  mpFacebook
//
//  Created by Gleb on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASMiniFacebookProfile;
@class ASTopStatusBarImageView;

typedef void(^ASBTLEManagerBlock)(ASMiniFacebookProfile *profile);

@class ASBTLECentral;
@class ASBTLEPeripheral;

@interface ASBTLEManager : NSObject

@property (nonatomic,retain) ASBTLECentral *central;
@property (nonatomic,retain) ASBTLEPeripheral *peripheal;
@property (nonatomic,retain) NSMutableArray *peripheralsArray;
@property (nonatomic,copy) ASBTLEManagerBlock newPeripheralHandler;

@property (nonatomic,retain) ASTopStatusBarImageView *statusView;

+ (ASBTLEManager *)sharedManager;

- (void)enablePeripheralMode;
- (void)enableCenralMode;
- (void)disblePeripheralMode;
- (void)disbleCenralMode;

- (void)sendRequestToDeviceWithUdid:(id)udid;

- (void)reciveAnsvwerFromUser:(NSString*)uudid withResult:(NSString*)result;
- (void)acceptUser:(NSString*)userUudid;
- (void)declineUser:(NSString*)userUudid;

@end
