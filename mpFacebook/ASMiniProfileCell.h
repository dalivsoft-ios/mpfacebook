//
//  ASMiniProfileCell.h
//  mpFacebook
//
//  Created by Gleb on 02.08.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ASMiniFacebookProfile;

@interface ASMiniProfileCell : UITableViewCell

@property (nonatomic,retain) ASMiniFacebookProfile *profile;

@end
