//
//  ASConnectToPerViewController.h
//  mpFacebook
//
//  Created by Gleb on 25.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASViewController.h"

@class ASMiniFacebookProfile;

@interface ASProfileViewController : ASViewController

@property (strong, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *ageLabel;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (nonatomic,assign) ASMiniFacebookProfile *profile;

@property (nonatomic,assign) BOOL isInvite;

- (IBAction)sendFriendRequest:(id)sender;

- (IBAction)cancelButtonDidClicked:(id)sender;

- (IBAction)noAcceptFriendRequestButtonDidClicked:(id)sender;

- (IBAction)acceptFriendRequestButtonDidClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *acceptFriendRequest;
@property (strong, nonatomic) IBOutlet UIButton *noAcceptFriendRequest;
@property (strong, nonatomic) IBOutlet UIButton *writeValue;
@property (strong, nonatomic) IBOutlet UIButton *cancel;
@property (strong, nonatomic) IBOutlet UILabel *questionsAddFriendsLabel;


@end
