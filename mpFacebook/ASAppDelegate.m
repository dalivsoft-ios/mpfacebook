//
//  ASAppDelegate.m
//  mpFacebook
//
//  Created by Vitaly Evtushenko on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASAppDelegate.h"

#import "ASLoginViewController.h"
#import "ASFacebookManager.h"
#import "ASCoreServices.h"
#import "BWQuincyManager.h"
#import "ASMiniFacebookProfile.h"
#import "ASProfileViewController.h"
#import "ASBTLEManager.h"

@implementation ASAppDelegate
{
    ASFacebookManager *facebookManager;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    APPLICATION_STARTED
    
    [[BWQuincyManager sharedQuincyManager] setSubmissionURL:@"http://crash.ashberrysoft.com/crash_v200.php"];
    [[BWQuincyManager sharedQuincyManager] setAutoSubmitCrashReport:YES];
    
    [ASBTLEManager sharedManager];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [ASLoginViewController new];
    self.nav = [[UINavigationController alloc] initWithRootViewController:self.viewController];
    self.window.rootViewController = self.nav;
    [self.window makeKeyAndVisible];
    
     facebookManager = [ASFacebookManager sharedManager];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

//- (void)applicationWillEnterForeground:(UIApplication *)application
//{
//    [FBSession.activeSession close];
//}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSession.activeSession handleDidBecomeActive];
    [FBSession setActiveSession:facebookManager.session];
    
    //[[ASFacebookManager sharedManager] sendCashedFriendRequests];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
   //[facebookManager.session close];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [facebookManager.session handleOpenURL:url];
}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    ASMiniFacebookProfile *user;
    if([notification userInfo][@"user"]){
        for(ASMiniFacebookProfile *profile in [[ASFacebookManager sharedManager] friendRequests]){
            if([profile.deviceUUID isEqualToString:[notification userInfo][@"deviceUUID"]]){
                user = profile;
            }
        }
    }
    if(user){
        user.deviceUUID = [notification userInfo][@"deviceUUID"];
        
        ASProfileViewController *cntr = [ASProfileViewController new];
        cntr.isInvite = YES;
        cntr.profile = user;
        [APPDEL.nav pushViewController:cntr animated:YES];

    }
    [application cancelLocalNotification:notification];
}

@end
