//
//  ASBTLEPeripheral.m
//  mpFacebook
//
//  Created by Gleb on 16.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASBTLEPeripheral.h"
#import "ASCoreServices.h"
#import "ASMiniFacebookProfile.h"
#import "ASFacebookManager.h"
#import "ASMiniFacebookProfile.h"
#import "ASAppDelegate.h"
#import "ASProfileViewController.h"
#import "UIDevice-Hardware.h"
#import "NSString+MD5Addition.h"
#import "ASBTLEManager.h"
#import "ASNetworkLog.h"

@implementation ASBTLEPeripheral
{
    NSMutableDictionary *_recivedData;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
        self.dataToSend = [[NSMutableData alloc] init];
        _recivedData = [[NSMutableDictionary alloc] init];
        
    }
    return self;
}

- (void)enableService
{
    if (self.service) {
        [self.peripheralManager removeService:self.service];
    }
    
    self.service = [[CBMutableService alloc] initWithType:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]
                                                  primary:YES];
    
    self.transferCharacteristics = [self _transferCharacteristics];
    self.service.characteristics = self.transferCharacteristics;
    
    [self.peripheralManager addService:self.service];
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral
            didAddService:(CBService *)service
                    error:(NSError *)error {
    // As soon as the service is added, we should start advertising.
    [self startAdvertising];
}


- (void)startAdvertising
{
    if (self.peripheralManager.isAdvertising) {
        [self.peripheralManager stopAdvertising];
    }
    NSDictionary *advertisment = @{
                                   CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]],CBAdvertisementDataLocalNameKey:[[[ASFacebookManager sharedManager]myProfile] firstName],CBCentralManagerScanOptionAllowDuplicatesKey:[NSNumber numberWithBool:NO]
                                   };

    [self.peripheralManager startAdvertising:advertisment];
}




- (NSArray*)_transferCharacteristics
{
    NSMutableData *data = [NSMutableData new];
    [data appendData:[[[ASFacebookManager sharedManager] myProfile] profileData]];
    [data appendData:[[[[UIDevice currentDevice] macaddress] md5] dataUsingEncoding:NSUTF8StringEncoding]];
    CBMutableCharacteristic *profileTransferCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:PROFILE]
                                                                                                properties:CBCharacteristicPropertyRead
                                                                                                     value:data
                                                                                               permissions:CBAttributePermissionsReadable];
    
    CBMutableCharacteristic *requestTransferCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:ADD_FRIEND_REQUEST]
                                                                                                properties:CBCharacteristicPropertyWriteWithoutResponse
                                                                                                     value:nil
                                                                                               permissions:CBAttributePermissionsWriteable];
    
    CBMutableCharacteristic *udidTransferCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:UDIID]
                                                                                               properties:CBCharacteristicPropertyRead
                                                                                                  value:[[[ASNetworkLog sharedLog] uniqueGlobalDeviceIdentifier] dataUsingEncoding:NSUTF8StringEncoding]
                                                                                              permissions:CBAttributePermissionsReadable];
    CBMutableCharacteristic *responseTransferCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:ADD_FRIEND_RESPONSE]
                                                                                             properties:CBCharacteristicPropertyWriteWithoutResponse
                                                                                                  value:nil
                                                                                            permissions:CBAttributePermissionsWriteable];
    
    return @[profileTransferCharacteristic,requestTransferCharacteristic,udidTransferCharacteristic,responseTransferCharacteristic];
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    peripheral.delegate = self;
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOn:
            NSLog(@"peripheralStateChange: Powered On");
            // As soon as the peripheral/bluetooth is turned on, start initializing
            // the service.
            [self enableService];
            break;
        case CBPeripheralManagerStatePoweredOff: {
            NSLog(@"peripheralStateChange: Powered Off");
            [self disableService];
            break;
        }
        case CBPeripheralManagerStateResetting: {
            NSLog(@"peripheralStateChange: Resetting");
            break;
        }
        case CBPeripheralManagerStateUnauthorized: {
            NSLog(@"peripheralStateChange: Deauthorized");
            [self disableService];
            break;
        }
        case CBPeripheralManagerStateUnsupported: {
            NSLog(@"peripheralStateChange: Unsupported");
            // TODO: Give user feedback that Bluetooth is not supported.
            break;
        }
        case CBPeripheralManagerStateUnknown:
            NSLog(@"peripheralStateChange: Unknown");
            break;
        default:
            break;
    }
    
}

- (void)disableService
{
    if(self.service){
        [self.peripheralManager removeService:self.service];
    }
    self.service = nil;
    [self stop];
}

#pragma mark

#pragma mark writed value

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray *)requests
{
    for(CBATTRequest * req in requests){
        if ([self compareCBUUID:req.characteristic.UUID UUID2:[CBUUID UUIDWithString:ADD_FRIEND_REQUEST]])
        {
            [self getProfile:req peripheral:peripheral];
        }
        if ([self compareCBUUID:req.characteristic.UUID UUID2:[CBUUID UUIDWithString:ADD_FRIEND_RESPONSE]])
        {
            [self getResponse:req peripheral:peripheral];
        }
    }
}

- (int) compareCBUUID:( CBUUID*) UUID1 UUID2:(CBUUID*)UUID2
{
    char b1[16];
    char b2[16];
    [UUID1.data getBytes:b1];
    [UUID2.data getBytes:b2];
    if (memcmp(b1, b2, UUID1.data.length) == 0)return 1;
    else return 0;
}

- (void) getResponse:(CBATTRequest *)req peripheral:(CBPeripheralManager *)peripheral
{
    NSString *deviceUUDID = [self _UUIDToString:req.central.UUID];
    NSData *response = req.value;
    [peripheral respondToRequest:req withResult:CBATTErrorSuccess];
    NSLog(@"%@",[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
    [[[UIAlertView alloc] initWithTitle:@"Response" message:[[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding] isEqualToString:@"1"]?@"Accepted":@"Rejected" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil] show];
    [[ASBTLEManager sharedManager] reciveAnsvwerFromUser:deviceUUDID withResult:[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]];
}

- (void) getProfile:(CBATTRequest *)req peripheral:(CBPeripheralManager *)peripheral
{
    NSString *deviceUUDID = [self _UUIDToString:req.central.UUID];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    if(![[[NSString alloc ] initWithData:req.value encoding:NSUTF8StringEncoding] isEqualToString:@"EOM"])
    {
        if(![_recivedData valueForKey:deviceUUDID]){
            [dict setValue:deviceUUDID forKey:@"deviceUUDID"];
            [dict setValue:req.value forKey:@"data"];
            [_recivedData setValue:dict forKey:deviceUUDID];
        }
        else{
            dict = _recivedData[deviceUUDID];
            NSMutableData *data = [[NSMutableData alloc] initWithData:dict[@"data"]];
            [data appendData:req.value];
            [dict setValue:data forKey:@"data"];
        }
        [peripheral respondToRequest:req withResult:CBATTErrorSuccess];
    }
    else{
        [self _profileDidRecived:_recivedData[deviceUUDID][@"data"] deviceUUDID:[self _UUIDToString:req.central.UUID]];
    }

}

#pragma mark

- (void)_profileDidRecived:(NSData*)profileData deviceUUDID:(NSString*)uudid
{
    ASMiniFacebookProfile *user = [[ASMiniFacebookProfile alloc] initWithRecivedData:profileData];
    user.deviceUUID = uudid;
    
    for (ASMiniFacebookProfile *prof in [[ASFacebookManager sharedManager] friendRequests]){
        if ([prof.userId isEqual:user.userId]) {
            return;
        }
    }
    
    [[[ASFacebookManager sharedManager] friendRequests] addObject:user];
    
    UIApplication *app = [UIApplication sharedApplication];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    if (notification) {
        notification.fireDate = [NSDate date];
        notification.timeZone = [NSTimeZone defaultTimeZone];
        
        NSString *notificationMessage = [NSString stringWithFormat:@"%@ want to add you to friend",user.firstName];
        notification.alertBody = notificationMessage;
        notification.soundName = UILocalNotificationDefaultSoundName;
        NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:profileData,@"user",uudid,@"deviceUUID",nil];
        notification.userInfo = infoDict;
        [app presentLocalNotificationNow:notification];
    }
}

- (void)stop
{
    [self.peripheralManager stopAdvertising];
}


- (NSString*) _UUIDToString:(CFUUIDRef)UUID
{
    if (!UUID)
        return nil;
    
    CFStringRef s = CFUUIDCreateString(NULL, UUID);
    
    return [[NSString alloc] initWithUTF8String:CFStringGetCStringPtr(s, 0)];
}


@end
