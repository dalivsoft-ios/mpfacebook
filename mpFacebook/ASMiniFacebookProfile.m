//
//  ASMiniFacebookProfile.m
//  mpFacebook
//
//  Created by Gleb on 15.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASMiniFacebookProfile.h"
#import "ASAppDelegate.h"
#import "NSString+md5.h"
#import "ASFacebookManager.h"

@implementation ASMiniFacebookProfile
{
    BOOL _internetIsReachable;
}

- (id)initWithDictionary:(NSDictionary*)dict
{
    self = [super init];
    if (self) {
        self.firstName = [NSString stringWithFormat:@"%@",dict[@"firstName"]];
        self.name = [NSString stringWithFormat:@"%@",dict[@"name"]];
        self.userId = [NSString stringWithFormat:@"%@",dict[@"id"]];
        self.avatarURL = dict[@"avatarURL"];
        self.birthDay = [NSString stringWithFormat:@"%@",dict[@"birthday"]];        
        if(dict[@"avatar"]){
            self.avatar = dict[@"avatar"];
        }
        if(dict[@"udid"]){
            self.udid = dict[@"udid"];
        }
        self.avatarURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal", self.userId];
        
        if(_internetIsReachable){
            [self getAvatar];
        
        } else {
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
            
        }
    }
    return self;
}

- (void) _checkNetworkStatus:(NSNotification *)notice
{
   	Reachability* curReach = [notice object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    if(curReach.currentReachabilityStatus != NotReachable){
        [self getAvatar];
    }
}

- (void)getAvatar
{
    __unsafe_unretained id myself = self;
    if(self.avatarURL.length > 0){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[myself avatarURL]]];
            UIImage *profilePic = [[UIImage alloc] initWithData:data];
            dispatch_async(dispatch_get_main_queue(), ^(){
                if(profilePic){
                    if(![myself avatar]){
                        @try {
                            [myself setAvatar: profilePic];
                        }
                        @catch (NSException *exception) {
                            
                        }
                        @finally {
                            
                        }
                    }
                }
            });
        });
        
    }
}


- (id)initWithRecivedData:(NSData*)rawData
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    NSArray *keys = @[@"name",@"id",@"birthday",@"firstName",@"udid"];
    
  // NSUInteger length = [rawData length];
    NSString *rawString = [[NSString alloc] initWithData:rawData
                                             encoding:NSUTF8StringEncoding];
    
    NSArray *values = [rawString componentsSeparatedByString:@"\n"];
    for (int i = 0; i < keys.count; i++) {
        [dict setValue:values[i] forKey:keys[i]];
    }
    _internetIsReachable = NO;
    if([[ASFacebookManager sharedManager] isInternetReachable])
    {
        _internetIsReachable = YES;
    }
        self = [self initWithDictionary:dict];
        if(self){
            
            [[NSNotificationCenter defaultCenter] addObserverForName:AVATAR_DID_LOAD object:nil
                                                               queue:nil usingBlock:^(NSNotification *notif){
                                                                   if([self.deviceUUID isEqualToString:[notif object][@"devideUUDID"]]){
                                                                       self.avatar = [notif object][@"image"];
                                                                   }
                                                               }];

        }
    return self;
}


-(NSData *)profileData
{
    NSMutableData *data = [[NSMutableData alloc] init];
    
    [data appendData:[self.name dataUsingEncoding:NSUTF8StringEncoding]];
    [data appendData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [data appendData:[self.userId dataUsingEncoding:NSUTF8StringEncoding]];
    [data appendData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [data appendData:[self.birthDay dataUsingEncoding:NSUTF8StringEncoding]];
    [data appendData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [data appendData:[self.firstName dataUsingEncoding:NSUTF8StringEncoding]];
    [data appendData:[@"\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    return data;
}

- (void)setDeviceUUID:(NSString*)deviceUUID
{
    _deviceUUID = deviceUUID;
}


- (void)setIsMyProfile:(BOOL)isMyProfile
{
    _isMyProfile = isMyProfile;
    if(_isMyProfile){
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:self.firstName,@"firstName",
                              self.name,@"name",
                              self.userId,@"id",
                              self.avatarURL,@"avatarURL",
                              self.birthDay,@"birthday",
                              nil];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        [dict writeToFile:[NSString stringWithFormat:@"%@/UserProfile",documentsDirectory]atomically:YES];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end
