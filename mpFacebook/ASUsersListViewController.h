//
//  ASUsersListViewController.h
//  mpFacebook
//
//  Created by Gleb on 15.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASViewController.h"

@interface ASUsersListViewController : ASViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,retain)IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *scanButton;

- (IBAction)scanStateButtonDidClicked:(id)sender;

@end
