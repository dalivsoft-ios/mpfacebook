//
//  ASMiniProfileCell.m
//  mpFacebook
//
//  Created by Gleb on 02.08.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASMiniProfileCell.h"
#import "ASMiniFacebookProfile.h"
#import "Reachability.h"

@implementation ASMiniProfileCell
{
    UIButton *avatarButton;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        avatarButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        [avatarButton setBackgroundImage:[UIImage imageNamed:@"facebook-logo"] forState:UIControlStateNormal];
        [avatarButton addTarget:self action:@selector(avatarButtonDidClicked) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:avatarButton];
        
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_checkNetworkStatus:) name:@"kNetworkReachabilityChangedNotification" object:nil];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if(_profile.approved){
        avatarButton.hidden = NO;
        avatarButton.center = CGPointMake(self.frame.size.width - 45, self.frame.size.height / 2);
        self.textLabel.textColor = [UIColor blueColor];
    } else {
        self.textLabel.textColor = [UIColor blackColor];
        avatarButton.hidden = YES;
    }
    if (_profile.canceled) {
        self.textLabel.textColor = [UIColor grayColor];
    }
}

- (void)setProfile:(ASMiniFacebookProfile *)profile
{
    _profile = profile;
    if(_profile.approved){
        if (_profile.avatar) {
            [avatarButton setBackgroundImage:profile.avatar forState:UIControlStateNormal];
            self.accessoryType = UITableViewCellAccessoryNone;
        } else {
            self.accessoryType = UITableViewCellAccessoryNone;
            [self.profile addObserver:self forKeyPath:@"avatar" options:0 context:nil];
            
        }
    }
    if (_profile.canceled) {
        self.textLabel.textColor = [UIColor grayColor];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"avatar"]){
        [avatarButton setBackgroundImage:self.profile.avatar forState:UIControlStateNormal];
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (void) _checkNetworkStatus:(NSNotification *)notice
{
   	Reachability* curReach = [notice object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    if(curReach.currentReachabilityStatus != NotReachable){
        [self.profile getAvatar];
    }
}


- (void)avatarButtonDidClicked
{
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",self.profile.userId]];
    
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    } else {
        url = [ [ NSURL alloc ] initWithString:[NSString stringWithFormat:@"http://m.facebook.com/%@",self.profile.userId]];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNetworkReachabilityChangedNotification" object:nil];

}

@end
