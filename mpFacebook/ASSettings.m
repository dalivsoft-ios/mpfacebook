//
//  ASSettings.m
//  ASCoreServices
//
//  Created by Vitaly Evtushenko on 08.05.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import "ASSettings.h"

static ASSettings *sharedInstance;

@implementation ASSettings

+ (ASSettings *)sharedSettings {
    return sharedInstance;
}

+ (void)initialize {
    NSAssert(self == [ASSettings class], @"Singleton is not designed to be subclassed.");
    sharedInstance = [[ASSettings alloc] init];
}

- (id)init {
    if ((self = [super init])) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        path = [[paths lastObject] stringByAppendingPathComponent:@"ASSettings.plist"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            data = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        } else {
            data = [[NSMutableDictionary alloc] init];
        }
    }
    return self;
}

- (id)valueForKey:(NSString *)key {
    return [data valueForKey:key];
}

- (void)setValue:(id)value forKey:(NSString *)key {
    [data setValue:value forKey:key];
    [data writeToFile:path atomically:YES];
}

- (void)removeValueForKey:(NSString *)key {
    [data removeObjectForKey:key];
    [data writeToFile:path atomically:YES];
}

@end
