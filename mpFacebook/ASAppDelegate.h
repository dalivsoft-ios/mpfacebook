//
//  ASAppDelegate.h
//  mpFacebook
//
//  Created by Vitaly Evtushenko on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <UIKit/UIKit.h>

#define DID_BEGIN_AUTH @"didBeginLoginNotification"
#define DID_END_AUTH @"didEndLoginNotification"
#define AVATAR_DID_LOAD @"avatarDidLoad"


#define APPDEL ((ASAppDelegate*)[[UIApplication sharedApplication]delegate])

@class ASViewController;

@interface ASAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ASViewController *viewController;

@property (strong, nonatomic) UINavigationController *nav;

@end
