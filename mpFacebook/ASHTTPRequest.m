//
//  ASHTTPGetRequest.m
//  autoparad
//
//  Created by Vitaly Evtushenko on 08.11.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import "ASHTTPRequest.h"
#import <MobileCoreServices/MobileCoreServices.h>

@implementation ASHTTPRequest
{
    NSURLConnection *_connection;
    NSMutableURLRequest *_request;
    NSMutableDictionary *_postParams;
}

- (id)initWithURL:(NSURL *)url
{
    if ((self = [super init])) {
        self.url = url;
        self.timeout = 30.0;
    }
    return self;
}

- (void)dealloc
{
    [_connection cancel];
    _connection = nil;
    _request = nil;
    _postParams = nil;
    
}

+ (ASHTTPRequest *)requestWithURL:(NSURL *)url
{
    return [[ASHTTPRequest alloc] initWithURL:url];
}

- (void)addRequestHeader:(NSString *)header value:(NSString *)value
{
    if (!self.requestHeaders) self.requestHeaders = [NSMutableDictionary dictionary];
    self.requestHeaders[header] = value;
}


#pragma mark setup request

- (void)addPostValue:(id <NSObject>)value forKey:(NSString *)key
{
	if (!key) {
		return;
	}
	if (![self postData]) {
		[self setPostData:[NSMutableArray array]];
	}
	NSMutableDictionary *keyValuePair = [NSMutableDictionary dictionaryWithCapacity:2];
	[keyValuePair setValue:key forKey:@"key"];
	[keyValuePair setValue:[value description] forKey:@"value"];
	[[self postData] addObject:keyValuePair];
}

- (void)setPostValue:(id <NSObject>)value forKey:(NSString *)key
{
	// Remove any existing value
	NSUInteger i;
	for (i=0; i<[[self postData] count]; i++) {
		NSDictionary *val = [[self postData] objectAtIndex:i];
		if ([[val objectForKey:@"key"] isEqualToString:key]) {
			[[self postData] removeObjectAtIndex:i];
			i--;
		}
	}
	[self addPostValue:value forKey:key];
}


- (void)addFile:(NSString *)filePath forKey:(NSString *)key
{
	[self addFile:filePath withFileName:nil andContentType:nil forKey:key];
}

+ (NSString *)mimeTypeForFileAtPath:(NSString *)path
{
	if (![[[NSFileManager alloc] init] fileExistsAtPath:path]) {
		return nil;
	}
	// Borrowed from http://stackoverflow.com/questions/2439020/wheres-the-iphone-mime-type-database
	CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[path pathExtension], NULL);
    CFStringRef MIMEType = UTTypeCopyPreferredTagWithClass (UTI, kUTTagClassMIMEType);
    CFRelease(UTI);
	if (!MIMEType) {
		return @"application/octet-stream";
	}
    NSString *res = [NSString stringWithString:(__bridge NSString *)MIMEType];
    CFRelease(MIMEType);
    return res;
}

- (void)addFile:(NSString *)filePath withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key
{
	BOOL isDirectory = NO;
	BOOL fileExists = [[[NSFileManager alloc] init] fileExistsAtPath:filePath isDirectory:&isDirectory];
	if (!fileExists || isDirectory) {
        return;
	}
    
	// If the caller didn't specify a custom file name, we'll use the file name of the file we were passed
	if (!fileName) {
		fileName = [filePath lastPathComponent];
	}
    
	// If we were given the path to a file, and the user didn't specify a mime type, we can detect it from the file extension
	if (!contentType) {
		contentType = [ASHTTPRequest mimeTypeForFileAtPath:filePath];
	}
	[self addData:filePath withFileName:fileName andContentType:contentType forKey:key];
}

- (void)setFile:(NSString *)filePath forKey:(NSString *)key
{
	[self setFile:filePath withFileName:nil andContentType:nil forKey:key];
}

- (void)setFile:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key
{
	// Remove any existing value
	NSUInteger i;
	for (i=0; i<[[self fileData] count]; i++) {
		NSDictionary *val = [[self fileData] objectAtIndex:i];
		if ([[val objectForKey:@"key"] isEqualToString:key]) {
			[[self fileData] removeObjectAtIndex:i];
			i--;
		}
	}
	[self addFile:data withFileName:fileName andContentType:contentType forKey:key];
}

- (void)addData:(NSData *)data forKey:(NSString *)key
{
	[self addData:data withFileName:@"file" andContentType:nil forKey:key];
}

- (void)addData:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key
{
	if (![self fileData]) {
		[self setFileData:[NSMutableArray array]];
	}
	if (!contentType) {
		contentType = @"application/octet-stream";
	}
    
	NSMutableDictionary *fileInfo = [NSMutableDictionary dictionaryWithCapacity:4];
	[fileInfo setValue:key forKey:@"key"];
	[fileInfo setValue:fileName forKey:@"fileName"];
	[fileInfo setValue:contentType forKey:@"contentType"];
	[fileInfo setValue:data forKey:@"data"];
    
	[[self fileData] addObject:fileInfo];
}

- (void)setData:(NSData *)data forKey:(NSString *)key
{
	[self setData:data withFileName:@"file" andContentType:nil forKey:key];
}

- (void)setData:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key
{
	// Remove any existing value
	NSUInteger i;
	for (i=0; i<[[self fileData] count]; i++) {
		NSDictionary *val = [[self fileData] objectAtIndex:i];
		if ([[val objectForKey:@"key"] isEqualToString:key]) {
			[[self fileData] removeObjectAtIndex:i];
			i--;
		}
	}
	[self addData:data withFileName:fileName andContentType:contentType forKey:key];
}

- (void)appendPostString:(NSString *)s
{
    [self.postBody appendData:[s dataUsingEncoding:NSUTF8StringEncoding]];
}

- (void)appendPostData:(NSData *)d
{
    [self.postBody appendData:d];
}

- (void)appendPostDataFromFile:(NSString *)path
{
    [self.postBody appendData:[NSData dataWithContentsOfFile:path]];
}

- (void)buildPostBodyFromParams
{
    self.postBody = [NSMutableData data];
	NSString *charset = (NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
	
	// We don't bother to check if post data contains the boundary, since it's pretty unlikely that it does.
	CFUUIDRef uuid = CFUUIDCreate(nil);
	CFStringRef uuidString = CFUUIDCreateString(nil, uuid);
	NSString *stringBoundary = [NSString stringWithFormat:@"0xKhTmLbOuNdArY-%@",(__bridge NSString*)uuidString];
    CFRelease(uuidString);
	CFRelease(uuid);
	
	[self addRequestHeader:@"Content-Type" value:[NSString stringWithFormat:@"multipart/form-data; charset=%@; boundary=%@", charset, stringBoundary]];
	
	[self appendPostString:[NSString stringWithFormat:@"--%@\r\n",stringBoundary]];
	
	// Adds post data
	NSString *endItemBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n",stringBoundary];
	NSUInteger i=0;
	for (NSDictionary *val in [self postData]) {
		[self appendPostString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",[val objectForKey:@"key"]]];
		[self appendPostString:[val objectForKey:@"value"]];
		i++;
		if (i != [[self postData] count] || [[self fileData] count] > 0) { //Only add the boundary if this is not the last item in the post body
			[self appendPostString:endItemBoundary];
		}
	}
	
	// Adds files to upload
	i=0;
	for (NSDictionary *val in [self fileData]) {
        
		[self appendPostString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", [val objectForKey:@"key"], [val objectForKey:@"fileName"]]];
		[self appendPostString:[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", [val objectForKey:@"contentType"]]];
		
		id data = [val objectForKey:@"data"];
		if ([data isKindOfClass:[NSString class]]) {
			[self appendPostDataFromFile:data];
		} else {
			[self appendPostData:data];
		}
		i++;
		// Only add the boundary if this is not the last item in the post body
		if (i != [[self fileData] count]) {
			[self appendPostString:endItemBoundary];
		}
	}
	
	[self appendPostString:[NSString stringWithFormat:@"\r\n--%@--\r\n",stringBoundary]];
	
#if DEBUG_FORM_DATA_REQUEST
	[self addToDebugBody:@"==== End of multipart/form-data body ====\r\n"];
#endif
}

- (NSString *)responseString
{
    return [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
}

- (void)startSynchronous
{
    [self startAsynchronous];
}

- (void)startAsynchronous
{
    _request = [[NSMutableURLRequest alloc] initWithURL:self.url
                                            cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                        timeoutInterval:self.timeout];
    
    if (self.postData || self.fileData) {
        [self buildPostBodyFromParams];
    }
    
    if (self.requestMethod) [_request setHTTPMethod:self.requestMethod];
    if (self.postBody) [_request setHTTPBody:self.postBody];
    
    for (NSString *field in self.requestHeaders.allKeys) {
        [_request addValue:self.requestHeaders[field] forHTTPHeaderField:field];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURLCache *cache = [NSURLCache sharedURLCache];
        NSCachedURLResponse *cachedResponse = nil;
        if (self.cacheEnabled) {
            cachedResponse = [cache cachedResponseForRequest:_request];
        } else {
            [cache removeCachedResponseForRequest:_request];
        }
        
        if (cachedResponse) {
//            NSLog(@"cached %@", _request.URL);
            self.urlResponse = (NSHTTPURLResponse *)cachedResponse.response;
            self.responseData = [NSMutableData dataWithData:cachedResponse.data];
            if (self.completionBlock) dispatch_async(dispatch_get_main_queue(), self.completionBlock);
        } else {
//            NSLog(@"starting %@", _request.URL);
            dispatch_async(dispatch_get_main_queue(), ^{
                _connection = [NSURLConnection connectionWithRequest:_request delegate:self];
            });
        }
        
    });
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    _connection = nil;
    _request = nil;
    self.error = error;
    if (self.failedBlock) dispatch_async(dispatch_get_main_queue(), self.failedBlock);
}

//- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection;
//- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge;

#pragma mark - NSURLConnectionDataDelegate

//- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response;

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
        self.urlResponse = (NSHTTPURLResponse *)response;
    }
    self.responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}

//- (NSInputStream *)connection:(NSURLConnection *)connection needNewBodyStream:(NSURLRequest *)request;

//- (void)connection:(NSURLConnection *)connection   didSendBodyData:(NSInteger)bytesWritten
// totalBytesWritten:(NSInteger)totalBytesWritten
//totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite;

//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse;

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSCachedURLResponse *cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:self.urlResponse data:self.responseData userInfo:nil storagePolicy:NSURLCacheStorageAllowed];
    [[NSURLCache sharedURLCache] storeCachedResponse:cachedResponse forRequest:_request];
    self.error = nil;
    if (self.completionBlock) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.completionBlock();
            _connection = nil;
            _request = nil;
        });
    }
}

@end
