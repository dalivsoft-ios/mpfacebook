//
//  ASBTLEPeripheral.h
//  mpFacebook
//
//  Created by Gleb on 16.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "TransferService.h"

@interface ASBTLEPeripheral : NSObject<CBPeripheralManagerDelegate,CBPeripheralDelegate>

@property(nonatomic,retain) CBPeripheralManager *peripheralManager;

@property(nonatomic,retain) NSData *dataToSend;
@property(nonatomic,assign) NSInteger sendDataIndex;
@property(nonatomic,retain) NSArray *transferCharacteristics;
@property(nonatomic, strong) CBMutableService *service;

- (void)startAdvertising;
- (void)stop;
- (void)enableService;


@end
