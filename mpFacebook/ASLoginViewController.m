//
//  ASLoginViewController.m
//  mpFacebook
//
//  Created by Gleb on 24.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASLoginViewController.h"
#import "ASUsersListViewController.h"
#import "ASFacebookManager.h"
#import "ASAppDelegate.h"
#import "ASBTLEManager.h"
#import "ASMiniFacebookProfile.h"

@interface ASLoginViewController ()
{
    UIActivityIndicatorView *_indicator;
    BOOL _didLogin;
}

@end

@implementation ASLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            _indicator.transform = CGAffineTransformMakeScale(4.75, 4.75);
        }
        [self.view addSubview:_indicator];
        
        _didLogin = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.title = @"Facebook";
    CGPoint center = CGPointMake(self.view.center.x, self.view.center.y - 44);
    _indicator.center = center;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:DID_BEGIN_AUTH object:nil queue:nil usingBlock:^(NSNotification *notif){
        [self _showLoadingInterface];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:DID_END_AUTH object:nil queue:nil usingBlock:^(NSNotification *notif){
        if([[notif object] boolValue]){
            [self performSelector:@selector(_startScanning) withObject:nil afterDelay:2.0];
        }
        else{
            [self performSelector:@selector(_hideLoadingInterface) withObject:nil afterDelay:1.0];
        }
    }];
    
    if([[ASFacebookManager sharedManager] isAuthorized]){
        ASUsersListViewController *cntr = [ASUsersListViewController new];
        cntr.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self.navigationController pushViewController:cntr animated:NO];
        [UIView transitionWithView:self.navigationController.view duration:1 options:UIViewAnimationOptionTransitionFlipFromRight animations:nil completion:nil];
    }
    
    self.navigationController.navigationBar.hidden = YES;
    if(!self.topStatusBarImageView){
        self.topStatusBarImageView = [[ASBTLEManager sharedManager] statusView];
        [self.view addSubview:self.topStatusBarImageView];
    }
    
    //[self performSelector:@selector(loadSavedProfIfHavenoIinternet) withObject:nil afterDelay:1];
}

- (void)loadSavedProfIfHavenoIinternet
{
    if(![[ASFacebookManager sharedManager] isInternetReachable])
    {
        [[ASFacebookManager sharedManager] getSavedProfile];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loginButtoButtonDidClicked:(UIButton*)sender
{
    if(![[ASFacebookManager sharedManager] isInternetReachable])
    {
        [[ASFacebookManager sharedManager] getSavedProfile];
    }
    else{
    [sender setSelected:YES];
    if(!_didLogin){
        [[ASFacebookManager sharedManager] loginWithFacebookWithSuccessBlock:^(id result){
            //[self _startScanning];
        } errorBlock:^(id error){}];
    }
    }
}

- (void)_startScanning
{
    _didLogin = YES;
    ASUsersListViewController *cntr = [ASUsersListViewController new];
    cntr.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    [self.navigationController pushViewController:cntr animated:NO];
    [UIView transitionWithView:self.navigationController.view duration:1 options:UIViewAnimationOptionTransitionFlipFromRight animations:nil completion:nil];
}

- (void)_showLoadingInterface
{
    [_indicator startAnimating];
    self.topStatusBarImageView.status = @"Connecting...";
    self.loginButton.hidden = YES;
    self.botAlert.hidden = YES;
}

- (void)_hideLoadingInterface
{
    [self.view bringSubviewToFront:self.contentView];
    [_indicator stopAnimating];
    self.loginButton.hidden = NO;
    self.botAlert.hidden = NO;
}

@end
