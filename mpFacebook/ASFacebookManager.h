//
//  ASFacebookManager.h
//  mpFacebook
//
//  Created by Gleb on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "FacebookSDK.framework/Versions/A/DeprecatedHeaders/Facebook.h"
#import "Reachability.h"

@class FBSession;
@class ASMiniFacebookProfile;

typedef void(^FacebookManagerSuccessBlock)(id result);
typedef void(^FacebookManagerErrorBlock)(id result);

@interface ASFacebookManager : NSObject<FBRequestDelegate,FBDialogDelegate>
{
    NSString *test;
    Reachability *internetReachable;
    Reachability *hostReachable;
}

@property (strong, nonatomic) FBSession *session;
@property (strong, nonatomic) Facebook *facebook;
@property (assign, nonatomic) BOOL isAuthorized;
@property (strong, nonatomic) NSMutableArray *friendRequests;
@property (strong, nonatomic) NSMutableArray *facebookCachedFriendsRequest;
@property (nonatomic,retain)  NSURL *openedURL;

- (void)addFirndsRequestWithUserId:(NSString*)userId;
- (void)removeFirndsRequestWithUserId:(NSString*)userId;
- (void)sendCashedFriendRequests;

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;

- (void)getSavedProfile;

+ (ASFacebookManager *)sharedManager;

- (void)loginWithFacebookWithSuccessBlock:(FacebookManagerSuccessBlock)successBlock errorBlock:(FacebookManagerErrorBlock)errorBlock;

- (void)getMyProfileWithSuccessBlock:(FacebookManagerSuccessBlock)successBlock errorBlock:(FacebookManagerErrorBlock)erroBlock;

- (ASMiniFacebookProfile*)myProfile;

- (BOOL)isInternetReachable;

- (void)inviteUser:(ASMiniFacebookProfile*)user;
- (void)inviteUserWithIds:(NSArray*)uid;

@end
