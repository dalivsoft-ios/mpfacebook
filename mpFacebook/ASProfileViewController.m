//
//  ASConnectToPerViewController.m
//  mpFacebook
//
//  Created by Gleb on 25.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASProfileViewController.h"
#import "ASBTLEManager.h"
#import "ASBTLECentral.h"
#import "ASMiniFacebookProfile.h"
#import "ASFacebookManager.h"
#import "ASAppDelegate.h"

@interface ASProfileViewController ()
{
    NSDateFormatter *_formatter;
}

@end

@implementation ASProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _formatter = [[NSDateFormatter alloc] init];
        _formatter.dateFormat = @"M/dd/YYY";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.topStatusBarImageView.status = self.profile.firstName;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self _getAvatar];
    
    self.title = self.profile.firstName;
    self.fullNameLabel.text = self.profile.name;
    
    [_formatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *bDate = [_formatter dateFromString:self.profile.birthDay];

    NSInteger years;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:kCFCalendarUnitYear
                                                        fromDate:bDate
                                                          toDate:[NSDate date]
                                                         options:0];
    years = [components year];
    self.ageLabel.text = [NSString stringWithFormat:@"%d",years];
    
    if(self.profile.avatar){
        [self.indicator setHidden:YES];
        self.avatarImageView.image = self.profile.avatar;
    }
    else{
        self.avatarImageView.image = [UIImage imageNamed:@"facebook-logo"];
        [self.profile addObserver:self forKeyPath:@"avatar" options:0 context:nil];
    }
    
    if(self.isInvite){
        [self _enableInviteMode];
    }
    
}

- (void)_getAvatar
{
#warning TODO://
}

- (void)_enableInviteMode
{
    
    [self.acceptFriendRequest setHidden:NO];
    [self.noAcceptFriendRequest setHidden:NO];
    [self.writeValue setHidden:YES];
    [self.cancel setHidden:YES];
    
    self.questionsAddFriendsLabel.text = [NSString stringWithFormat:@"%@ %@ %@",@"Add ",self.profile.firstName ,@" to friends?"];
    self.questionsAddFriendsLabel.textColor = [UIColor colorWithRed:0.188080 green:0.301206 blue:0.636966 alpha:1.000000];
    [self.questionsAddFriendsLabel setHidden:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark request
- (void)sendFriendRequest:(id)sender
{
        //[[ASFacebookManager sharedManager] addFirndsRequestWithUserId:self.profile.userId];
        [[[ASBTLEManager sharedManager] central] writeDataToDeviceWithUdid:self.profile.deviceUUID];
        [self _pop];
    
}

#pragma mark send response

- (void)noAcceptFriendRequestButtonDidClicked:(id)sender
{
     [[[ASBTLEManager sharedManager] central] sendResponseWithAccept:NO uudid:self.profile.deviceUUID];
    [[[ASFacebookManager sharedManager] friendRequests] removeObject:self.profile];
    [self _pop];
}

- (void)acceptFriendRequestButtonDidClicked:(id)sender
{
     [[[ASBTLEManager sharedManager] central] sendResponseWithAccept:YES uudid:self.profile.deviceUUID];
    [[[ASFacebookManager sharedManager] friendRequests] removeObject:self.profile];
    [self _pop];
}

#pragma mark

- (void)cancelButtonDidClicked:(id)sender
{
    [self _pop];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"avatar"]){
        [self.indicator setHidden:YES];
        self.avatarImageView.image = self.profile.avatar;
    }
}

- (void)_pop
{
    [self.navigationController popToViewController:[self.navigationController viewControllers][[[self.navigationController viewControllers ] indexOfObject:self] - 1] animated:YES];
}

@end
