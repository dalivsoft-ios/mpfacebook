//
//  ASHTTPGetRequest.h
//  autoparad
//
//  Created by Vitaly Evtushenko on 08.11.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ASHTTPGetRequestVoidBlock)();

@interface ASHTTPRequest : NSObject <NSURLConnectionDataDelegate>

@property (nonatomic, assign) NSInteger tag;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSMutableDictionary *requestHeaders;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, copy) ASHTTPGetRequestVoidBlock failedBlock;
@property (nonatomic, copy) ASHTTPGetRequestVoidBlock completionBlock;
@property (nonatomic, assign) NSTimeInterval timeout;
@property (nonatomic, strong) NSHTTPURLResponse *urlResponse;
@property (nonatomic, strong) NSString *requestMethod;
@property (nonatomic, strong) NSMutableData *postBody;
@property (nonatomic, assign) BOOL cacheEnabled;

@property (strong) NSMutableArray *postData;
@property (strong) NSMutableArray *fileData;

- (id)initWithURL:(NSURL *)url;

+ (ASHTTPRequest *)requestWithURL:(NSURL *)url;

- (void)addRequestHeader:(NSString *)header value:(NSString *)value;

- (NSString *)responseString;

- (void)startAsynchronous;

- (void)startSynchronous;

- (void)addPostValue:(id)value forKey:(NSString *)key;

@end
