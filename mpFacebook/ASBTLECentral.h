//
//  ASBTLECentral.h
//  mpFacebook
//
//  Created by Gleb on 16.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "TransferService.h"

typedef NS_ENUM(NSUInteger, ASBTLECentralMode)
{
    ASBTLECentralModeScann = 0,
    ASBTLECentralModeReadData,
    ASBTLECentralModeWriteData,
    ASBTLECentralModeSendingResponse,
};

typedef enum{
    ASBTLEManagerDirectConnectionStateStarted = 0,
    ASBTLEManagerDirectConnectionStateFinished,
    ASBTLEManagerDirectConnectionStateFailed,
}ASBTLCentralDirectConnectionState;

@class ASMiniFacebookProfile;

typedef void(^ASBTLECentralBlock)(ASMiniFacebookProfile *profile);
typedef void(^ASBTLECentralOperationsBlock)();

@interface ASBTLECentral : NSObject<CBCentralManagerDelegate,CBPeripheralDelegate>

@property(nonatomic,retain) CBCentralManager *centralManager;
@property(nonatomic,retain) CBPeripheral *connectedPeripheral;

@property(nonatomic,assign) ASBTLECentralMode mode;

@property (nonatomic,copy) ASBTLECentralBlock newDeviseDetected;

@property (nonatomic,assign)ASBTLCentralDirectConnectionState directConnectionState;

- (void)startScan;

- (void)stopScan;

- (void)disconectCurrentConnection;

- (void)readDataFromDeviceWithUdid:(NSString*)udid;

- (void)writeDataToDeviceWithUdid:(NSString*)udid;

- (void)cleanup;

- (void)sendResponseWithAccept:(BOOL)isAccept uudid:(NSString*)uudid;

@end
