//
//  ASCoreServices.h
//  ASCoreServices
//
//  Created by Vitaly Evtushenko on 08.05.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import "ASNetworkLog.h"
#import "ASSettings.h"
#import "UIDevice+version.h"

#ifdef DEBUG
#define ASLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define ASLog(...) 
#endif
