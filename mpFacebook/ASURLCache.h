//
//  ASURLCache.h
//  autoparad
//
//  Created by Vitaly Evtushenko on 08.11.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

//store time in seconds

#define ASURLCacheStoreTime 14400

@interface ASURLCache : NSURLCache

@end
