//
//  UIDevice+version.m
//  sweetgift-ipad
//
//  Created by Vitaly Evtushenko on 25.10.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import "UIDevice+version.h"

@implementation UIDevice (version)

- (BOOL)iOSVersionIsAtLeast:(NSString*)version {
    NSComparisonResult result = [[self systemVersion] compare:version options:NSNumericSearch];
    return (result == NSOrderedDescending || result == NSOrderedSame);
}

@end
