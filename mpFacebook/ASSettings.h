//
//  ASSettings.h
//  ASCoreServices
//
//  Created by Vitaly Evtushenko on 08.05.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define ASSettingsGetValueForKey(key) [[ASSettings sharedSettings] valueForKey:key]
#define ASSettingsSetValueForKey(value, key) [[ASSettings sharedSettings] setValue:value forKey:key]
#define ASSettingsRemoveValueForKey(key) [[ASSettings sharedSettings] removeValueForKey:key]

@interface ASSettings : NSObject {
    @private
    NSMutableDictionary *data;
    NSString *path;
}

+ (ASSettings *)sharedSettings;

- (void)removeValueForKey:(NSString *)key;

@end
