//
//  NSString+md5.h
//  FTUtils
//
//  Created by andrey on 07.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString(md5)


- (NSString*)md5HexDigest;
- (NSString *)urlEncodeValue;
- (NSString*)clearString;
- (NSString*)sha1HexDigest;

@end
