//
//  NSString+md5.m
//  FTUtils
//
//  Created by andrey on 07.02.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSString+md5.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString(md5)


- (NSString*)md5HexDigest
{
    const char *cStr = [self UTF8String];
	unsigned char result[16];
	CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
	return [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],result[8], result[9], result[10], result[11],result[12], result[13], result[14], result[15]];
}

- (NSString *)urlEncodeValue
{
	CFStringRef s = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)self, NULL, CFSTR("?=&+"), kCFStringEncodingUTF8);
    NSString *result = [NSString stringWithString:(__bridge NSString *)s];
    CFRelease(s);
	return result;
}

- (NSString*)clearString
{
	NSMutableString *s = [NSMutableString stringWithString:self];
	NSUInteger slen = [s length], loc;
	for (loc=0; loc < slen; loc++)
	{
		unichar c = [s characterAtIndex:loc];
		
		if (c == '\n' || c == '\r' || c < 0x20) {
			// Forward check for additional newlines
			unsigned int len = 1;
			[s replaceCharactersInRange:NSMakeRange(loc, len) withString:@""];
			slen -= 1; // s is now shorter so let's update slen
			loc -= 1;
		}
	}
	
	return s;
}

- (NSString*)sha1HexDigest
{
    const char *cstr = [self cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:self.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (CC_LONG)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}


@end
