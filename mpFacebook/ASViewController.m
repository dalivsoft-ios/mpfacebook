//
//  ASViewController.m
//  mpFacebook
//
//  Created by Vitaly Evtushenko on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASViewController.h"
#import "ASAppDelegate.h"
#import "ASFacebookManager.h"
#import "ASBTLEManager.h"

@interface ASViewController ()

@end

@implementation ASViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]];
    self.navigationController.navigationBar.hidden = YES;
    if ([[UIScreen mainScreen] bounds].size.height == 568)
    {
        UIImage * backgroundImage = [UIImage imageNamed:@"bg-568h"];
        self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!self.topStatusBarImageView){
        self.topStatusBarImageView = [[ASBTLEManager sharedManager] statusView];
        [self.view addSubview:self.topStatusBarImageView];
    }
    [self.view addSubview:self.topStatusBarImageView];
    [self.view bringSubviewToFront:self.topStatusBarImageView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
