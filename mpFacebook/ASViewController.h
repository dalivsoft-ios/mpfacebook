//
//  ASViewController.h
//  mpFacebook
//
//  Created by Vitaly Evtushenko on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASTopStatusBarImageView.h"

@interface ASViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet ASTopStatusBarImageView *topStatusBarImageView;

@end
