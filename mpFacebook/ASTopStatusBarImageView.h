//
//  ASTopStatusBarImageView.h
//  mpFacebook
//
//  Created by Gleb on 27.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASTopStatusBarImageView : UIImageView

@property (nonatomic,retain) UILabel *statusLabel;
@property (nonatomic,retain) NSString *status;
@property (nonatomic,retain) UIImageView *logoImageView;
@property (nonatomic,assign) BOOL isLogoEnabled;


@end
