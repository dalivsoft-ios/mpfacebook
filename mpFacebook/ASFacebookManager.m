//
//  ASFacebookManager.m
//  mpFacebook
//
//  Created by Gleb on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASFacebookManager.h"
#import "ASCoreServices.h"
#import "ASMiniFacebookProfile.h"
#import "ASAppDelegate.h"

@interface Facebook (Private)
- (void)authorizeWithFBAppAuth:(BOOL)tryFBAppAuth
                    safariAuth:(BOOL)trySafariAuth;
@end

@interface Facebook (MyApp)
- (void)myAuthorize:(id<FBSessionDelegate>)delegate;
@end

@implementation Facebook (MyApp)

- (void)myAuthorize:(id<FBSessionDelegate>)delegate {
    _sessionDelegate = delegate;
    [self authorizeWithFBAppAuth:NO safariAuth:NO]; // force in app auth
}
@end

@implementation ASFacebookManager
{
    ASMiniFacebookProfile *_myProfile;
    
    FacebookManagerSuccessBlock _authorizationSuccessBlock;
    FacebookManagerErrorBlock _authorizationErrorBlock;
}


static ASFacebookManager *sharedInstance = nil;

+ (ASFacebookManager *)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ASFacebookManager alloc] initInstance];
    });
    return sharedInstance;
}

- (id)initInstance
{
    if ((self = [super init])) {
        if (!self.session.isOpen) {
            self.session = [[FBSession alloc] initWithPermissions:@[@"email",@"user_birthday",@"user_photos"]];
            FBSession.activeSession = self.session;
            if(FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
                [self.session openWithCompletionHandler:^(FBSession *session,
                                                          FBSessionState status,
                                                          NSError *error) {
                    [self sessionStateChanged:session state:status error:error];
                }];
            }
            self.friendRequests = [[NSMutableArray alloc] init];
            self.facebookCachedFriendsRequest = [[NSMutableArray alloc] init];
            [self _getCachedRequests];
            [self _enableReachability];
        }
        
    }
    return self;
}

#pragma  mark internet reachability

- (void)_enableReachability
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    
    hostReachable = [Reachability reachabilityWithHostName: @"www.facebook.com"] ;
    [hostReachable startNotifier];
    
    internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
}

- (void) _checkNetworkStatus:(NSNotification *)notice
{
   	Reachability* curReach = [notice object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
    
}

- (BOOL)isInternetReachable
{
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            return NO;
            break;
        }
        case ReachableViaWiFi:
        case ReachableViaWWAN:
        {
            
            return YES;
            break;
        }
    }
    return NO;
}

#pragma mark

#pragma actions

- (void)loginWithFacebookWithSuccessBlock:(FacebookManagerSuccessBlock)successBlock errorBlock:(FacebookManagerErrorBlock)errorBlock
{
    _authorizationSuccessBlock = [successBlock copy];
    _authorizationErrorBlock = [errorBlock copy];
    
    if (self.session.state  != FBSessionStateCreated) {
        self.session = [[FBSession alloc] initWithPermissions:@[@"email",@"user_birthday",@"user_photos"]];
    }
    FBSession.activeSession = self.session;
    [FBSession.activeSession openWithCompletionHandler:^(FBSession *session,
                                                         FBSessionState status,
                                                         NSError *error) {
        [self sessionStateChanged:session state:status error:error];
    }];
}

- (void)getSavedProfile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *userProfilePatch = [NSString stringWithFormat:@"%@/UserProfile",documentsDirectory];
    
    NSDictionary *profile = [[NSDictionary alloc]initWithContentsOfFile:userProfilePatch];
    
    if(profile){
        self.isAuthorized = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:DID_BEGIN_AUTH object:nil];
        _myProfile = [[ASMiniFacebookProfile alloc]initWithDictionary:profile];
        [[NSNotificationCenter defaultCenter] postNotificationName:DID_END_AUTH object:@YES];
        return;
    }
}

-(void)getMyProfileWithSuccessBlock:(FacebookManagerSuccessBlock)successBlock errorBlock:(FacebookManagerErrorBlock)erroBlock
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *userProfilePatch = [NSString stringWithFormat:@"%@/UserProfile",documentsDirectory];
    
    NSDictionary *profile = [[NSDictionary alloc]initWithContentsOfFile:userProfilePatch];
    
    if(profile){
        [self getSavedProfile];
        return;
    }
    [FBRequestConnection
     startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                       id<FBGraphUser> user,
                                       NSError *error) {
         if (!error) {
             
             self.isAuthorized = YES;
             
             NSString *fbuid = [NSString stringWithFormat:@"%@",user.id];
             
             NSDictionary *profileInfo = [[NSDictionary alloc] initWithObjectsAndKeys:user.id,@"id",user.name,@"name",user.first_name,@"firstName",user.last_name,@"secondName",[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?", fbuid],@"avatarURL", user.birthday,@"birthday",nil];
             if(profileInfo){
                 _myProfile = [[ASMiniFacebookProfile alloc]initWithDictionary:profileInfo];
                 _myProfile.isMyProfile = YES;
             }
             
             if(_authorizationSuccessBlock){
                 _authorizationSuccessBlock(@"1");
             }
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
                 NSString *fbuid = [NSString stringWithFormat:@"%@",user.id];
                 NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=normal", fbuid]];
                 NSData *data = [NSData dataWithContentsOfURL:url];
                 UIImage *profilePic = [[UIImage alloc] initWithData:data];
                 dispatch_async(dispatch_get_main_queue(), ^(){
                     _myProfile.avatar = profilePic;
                     [[NSNotificationCenter defaultCenter] postNotificationName:DID_END_AUTH object:@YES];
                 });
             });
         }
         else{
             [[NSNotificationCenter defaultCenter] postNotificationName:DID_END_AUTH object:@NO];
             if(_authorizationErrorBlock){
                 _authorizationErrorBlock(error);
             }
         }
     }];
}

- (void)inviteUser:(ASMiniFacebookProfile *)user
{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:@"Hi please add me as a friend.", @"message", [NSString stringWithFormat:@"%@",user.userId], @"to", nil];
    [[self facebook] dialog: @"apprequests"
                  andParams: [params mutableCopy]
                andDelegate: self];
}

- (void)inviteUserWithIds:(NSArray*)uids
{
    int i = 0;
    NSMutableString *uidsString = [[NSMutableString alloc] init];
    for(NSString *uid in uids)
    {
        if(i == 0)
        {
            [uidsString appendFormat:@"%@",uid];
        }
        else
        {
            [uidsString appendFormat:@", %@",uid];
        }
        i++;
    }

    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:@"Hi please add me as a friend.", @"message", uidsString, @"to", nil];
    [[self facebook] dialog: @"apprequests"
                  andParams: [params mutableCopy]
                andDelegate: self];
}

#pragma mark

#pragma mark FbSessionDelegate

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if(!error)
            {
                if(_session.isOpen)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:DID_BEGIN_AUTH object:nil];
                    
                    self.facebook = [[Facebook alloc]
                                     initWithAppId:FBSession.activeSession.appID
                                     andDelegate:nil];
                    //[self.facebook myAuthorize:self];
                    self.facebook.accessToken = FBSession.activeSession.accessToken;
                    self.facebook.expirationDate = FBSession.activeSession.expirationDate;
                    
                    [self getMyProfileWithSuccessBlock:^(id result){} errorBlock:^(id error){}];
                }
                else
                {
                    [self fbDidReciveError:nil];
                }
            }
            else{
                [self fbDidReciveError:error];
            }
            
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:{
            [FBSession.activeSession closeAndClearTokenInformation];
            break;
        }
        default:
            break;
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    return [FBSession openActiveSessionWithReadPermissions:nil
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,
                                                             FBSessionState state,
                                                             NSError *error) {
                                             [self sessionStateChanged:session
                                                                 state:state
                                                                 error:error];
                                         }];
}
- (void)fbDidReciveError:(NSError*)error
{
    if(error)
    {
        NSString *errorDescr = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        ASLog(@"%@",errorDescr);
        errorDescr = nil;
        
        if(_authorizationErrorBlock){
            _authorizationErrorBlock(error);
        }
    }
    ASLog(@"fbSessionInvalidated");
}


#pragma mark - FBRequestDelegate methods

- (void)request:(FBRequest *)request didLoad:(id)result {
    
    NSLog(@"Result: %@", result);
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error
{
    
}


- (ASMiniFacebookProfile *)myProfile
{
    return _myProfile;
}

#pragma mark cached request

- (void)_getCachedRequests
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *userProfilePatch = [NSString stringWithFormat:@"%@/FacebookFriensdRequest",documentsDirectory];
    
    NSMutableDictionary *requests = [[NSMutableDictionary alloc]initWithContentsOfFile:userProfilePatch];
    if(requests.count == 0){
        return;
    }
    if([requests[@"usersId"] count] > 0){
        self.facebookCachedFriendsRequest = requests[@"usersId"];
    }
}

- (void)addFirndsRequestWithUserId:(NSString*)userId
{
    if(![self.facebookCachedFriendsRequest containsObject:userId]){
        [self.facebookCachedFriendsRequest addObject:userId];
        [self _saveCurrentStateFBRecuests];
    }
}

- (void)removeFirndsRequestWithUserId:(NSString*)userId
{
    if([self.facebookCachedFriendsRequest containsObject:userId]){
        [self.facebookCachedFriendsRequest removeObject:userId];
        [self _saveCurrentStateFBRecuests];
    }
}

- (void)_saveCurrentStateFBRecuests
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *userProfilePatch = [NSString stringWithFormat:@"%@/FacebookFriensdRequest",documentsDirectory];
    
    NSMutableDictionary *requests = [[NSMutableDictionary alloc] init];
    [requests setValue:self.facebookCachedFriendsRequest forKey:@"usersId"];
    
    [requests writeToFile:userProfilePatch atomically:YES];

}

- (void)sendCashedFriendRequests
{
    if ([self isInternetReachable]) {
        if(self.facebookCachedFriendsRequest.count > 0){
            NSMutableArray *uids = [[NSMutableArray alloc] init];
            for(NSString *uid in self.facebookCachedFriendsRequest){
                [uids addObject:uid];
                [self removeFirndsRequestWithUserId:uid];
            }
            [self inviteUserWithIds:uids];
        }
    }
}



- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}

@end
