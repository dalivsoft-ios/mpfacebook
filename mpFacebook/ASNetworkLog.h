//
//  ASNetworkLog.h
//  ASCoreServices
//
//  Created by Vitaly Evtushenko on 08.05.12.
//  Copyright (c) 2012 AshberrySoft LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#define ASNetLog(fmt, ...) [[ASNetworkLog sharedLog] log:[NSString stringWithFormat:fmt,##__VA_ARGS__]];
#define APPLICATION_STARTED ASNetLog(@"start");
#define APPLICATION_ACTIVE ASNetLog(@"active");

@interface ASNetworkLog : NSObject {
    NSString *uuid;
    BOOL crashHandlerInstalled;
}

+ (ASNetworkLog *)sharedLog;

- (void)log:(NSString *)message;

- (NSString *)uniqueGlobalDeviceIdentifier;

@end
