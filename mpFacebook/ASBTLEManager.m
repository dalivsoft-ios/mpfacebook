//
//  ASBTLEManager.m
//  mpFacebook
//
//  Created by Gleb on 01.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASBTLEManager.h"
#import "ASFacebookManager.h"
#import "ASBTLECentral.h"
#import "ASBTLEPeripheral.h"
#import "ASAppDelegate.h"
#import "ASMiniFacebookProfile.h"
#import "ASTopStatusBarImageView.h"

@implementation ASBTLEManager

static ASBTLEManager *sharedInstance = nil;

+ (ASBTLEManager *)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ASBTLEManager alloc] initInstance];
    });
    return sharedInstance;
}

- (id)initInstance
{
    self.peripheralsArray = [[NSMutableArray alloc] init];
    if ((self = [super init])) {
        self.peripheralsArray = [[NSMutableArray alloc] init];
        self.central = [[ASBTLECentral alloc] init];
        
        [self.central addObserver:self forKeyPath:@"directConnectionState" options:0 context:nil];
        
        __block ASBTLEManager *myself = self;
        [self.central setNewDeviseDetected:^(ASMiniFacebookProfile *profile){
                [myself _adddNewPeripheral:profile];
        }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:DID_END_AUTH object:nil queue:nil usingBlock:^(NSNotification *notif){
            if([[notif object] boolValue]){
                [myself enablePeripheralMode];
            }
            else{
            }
        }];
        
        self.statusView = [ASTopStatusBarImageView new];
        
        }
    return self;
}

- (void)_adddNewPeripheral:(ASMiniFacebookProfile*)profile
{
    for(ASMiniFacebookProfile *_profile in self.peripheralsArray){
        if (_profile.deviceUUID == profile.deviceUUID) {
            return;
        }
    }
    [self.peripheralsArray addObject:profile];
        ASBTLEManagerBlock block= [self newPeripheralHandler];
        block(profile);
}


- (void)enablePeripheralMode
{
    self.peripheal = [[ASBTLEPeripheral alloc] init];
}

- (void)disblePeripheralMode
{
    [self.peripheal stop];
}

- (void)enableCenralMode
{
    [self.central startScan];
}

- (void)disbleCenralMode
{
    [self.central cleanup];
    [self.central stopScan];
}

- (void)sendRequestToDeviceWithUdid:(id)udid
{
    [self.central readDataFromDeviceWithUdid:udid];
}

- (void)reciveAnsvwerFromUser:(NSString*)uudid withResult:(NSString*)result
{
    if([result isEqualToString:@"1"]){
        [self acceptUser:uudid];
    } else{
        [self declineUser:uudid];
    }
}

- (void)acceptUser:(NSString*)userUudid
{
    for(ASMiniFacebookProfile *profile in self.peripheralsArray){
        if ([profile.deviceUUID isEqual:userUudid]) {
            profile.approved = YES;
        }
    }
    self.newPeripheralHandler(nil);
}

- (void)declineUser:(NSString*)userUudid
{
    for(ASMiniFacebookProfile *profile in self.peripheralsArray){
        if ([profile.deviceUUID isEqual:userUudid]) {
            profile.canceled = YES;
        }
    }
    self.newPeripheralHandler(nil);
}

@end
