//
//  ASMiniFacebookProfile.h
//  mpFacebook
//
//  Created by Gleb on 15.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASMiniFacebookProfile : NSObject

@property (nonatomic,retain) NSString *userId;
@property (nonatomic,retain) NSString *name;
@property (nonatomic,retain) NSString *firstName;
@property (nonatomic,retain) NSString *secondName;
@property (nonatomic,retain) NSString *birthDay;
@property (atomic,retain) UIImage *avatar;
@property (nonatomic,retain) NSString *avatarURL;
@property (nonatomic,retain) NSString *deviceUUID;
@property (nonatomic,assign) BOOL isMyProfile;
@property (nonatomic,retain) NSString *udid;
@property (nonatomic,assign) BOOL approved;
@property (nonatomic,assign) BOOL canceled;

- (id)initWithDictionary:(NSDictionary*)dict;

- (id)initWithRecivedData:(NSData*)data;


- (NSData*)profileData;

- (void)getAvatar;

@end
