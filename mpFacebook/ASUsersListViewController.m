//
//  ASUsersListViewController.m
//  mpFacebook
//
//  Created by Gleb on 15.04.13.
//  Copyright (c) 2013 Vitaly Evtushenko. All rights reserved.
//

#import "ASUsersListViewController.h"
#import "ASBTLEManager.h"
#import "ASMiniFacebookProfile.h"
#import "ASProfileViewController.h"
#import "ASBTLEManager.h"
#import "ASBTLECentral.h"
#import "ASFacebookManager.h"
#import "ASMiniProfileCell.h"

@interface ASUsersListViewController ()
{
    NSMutableArray *_usersArray;
}

@end

@implementation ASUsersListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _usersArray = [[NSMutableArray alloc] init];
        _usersArray = [[ASBTLEManager sharedManager] peripheralsArray];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.hidesBackButton = YES;

    [[ASBTLEManager sharedManager] setNewPeripheralHandler:^(ASMiniFacebookProfile *profile){
        NSLog(@"New peripheral in table View!!!!!!!!!!");
        _usersArray = [[ASBTLEManager sharedManager] peripheralsArray];
        dispatch_async(dispatch_get_main_queue(), ^(){
            [self.tableView reloadData];
        });
    }];
    [self scanStateButtonDidClicked:self.scanButton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.topStatusBarImageView.status = @"Scan";
    [[ASFacebookManager sharedManager] sendCashedFriendRequests];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _usersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ASMiniProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell){
        cell = [[ASMiniProfileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //[_usersArray[indexPath.row] setApproved:YES];
    cell.profile = _usersArray[indexPath.row];
    cell.textLabel.text = [_usersArray[indexPath.row] firstName];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 88;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ASMiniFacebookProfile *prof = _usersArray[indexPath.row];
    if(!prof.approved && !prof.canceled) {
        ASProfileViewController *cntr = [ASProfileViewController new];
        cntr.profile = prof;
        [self.navigationController pushViewController:cntr animated:YES];
    } else if (prof.approved){
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",prof.userId]];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        } else {
            url = [ [ NSURL alloc ] initWithString:[NSString stringWithFormat:@"http://m.facebook.com/%@",prof.userId]];
            [[UIApplication sharedApplication] openURL:url];
        }

    }
}

#pragma mark

- (void)scanStateButtonDidClicked:(id)sender
{
    if(self.scanButton.selected){
        [[ASBTLEManager sharedManager] disbleCenralMode];
    }
    else{
        [[ASBTLEManager sharedManager] enableCenralMode];
    }
    self.scanButton.selected = !self.scanButton.selected;
}

@end
